$(function() {
    "use strict";
    if ($("#contact-form").length > 0) {
        $("#contact-form").validationEngine('attach', {
            scroll: false,
            onValidationComplete: function(form, status) {
                if (status === true) {
                    $('#form-submit-wrapper').empty();
                    $('#form-submit-wrapper').addClass('loading');
                    var form_data = $('#contact-form').serialize();
                    $('#btn_submit').attr({
                        disabled: 'disabled',
                        value: 'ENVIANDO...'
                    });
                    $.post('contacto-submit', form_data, function(data) {
                        $('#form-submit-wrapper').html(data);
                        $('#form-submit-wrapper').removeClass('loading');
                        $('#btn_submit').attr({
                            disabled: 'disabled',
                            value: 'LISTO!'
                        });
                    });
                }
            }
        });
    }
});
$(document).ready(function() {
    "use strict";
    $("#app-aside").sticky({
        topSpacing: 100,
        bottomSpacing: 850
    });
    $("body").niceScroll({
        cursorcolor: '#F00',
        cursorborder: '0px',
        cursorborderradius: '0px',
        background: '#000',
        hwacceleration: false,
        scrollspeed: 60,
        mousescrollstep: 85,
        smoothscroll: true,
        autohidemode: false,
        zindex: '99999999'
    });
    $("#search").validationEngine();
    $("#nwsl-email").validationEngine();
    var $container = $('#portafolio-container').imagesLoaded(function() {
        $container.isotope({
            itemSelector: '.portfolio-item',
            getSortData: {
                name: '.name'
            },
            sortBy: 'name'
        });
    });
});
$('#all').click(function() {
    "use strict";
    $('#portafolio-container').isotope({
        filter: '*',
        sortBy: 'name'
    });
});
$('#desarrollo-web').click(function() {
    "use strict";
    $('#portafolio-container').isotope({
        filter: '.desarrollo-web',
        sortBy: 'name'
    });
});
$('#tienda-online').click(function() {
    "use strict";
    $('#portafolio-container').isotope({
        filter: '.tienda-online',
        sortBy: 'name'
    });
});
var wHeight = 0;
var navbarstick = false;
$(window).scroll(function() {
    "use strict";
    wHeight = $(window).scrollTop();
    if (wHeight > 700) {
        if (navbarstick === false) {
            $("#sticker").sticky({
                topSpacing: 0
            });
        }
        navbarstick = true;
    } else {
        $("#sticker").unstick();
        navbarstick = false;
    }
});
var $grid = $('.blog-container').masonry({
    columnWidth: '.blog-item',
    itemSelector: '.blog-item'
});
$grid.imagesLoaded().progress(function() {
    "use strict";
    $grid.masonry('layout');
});
$('.reference-gallery').flickity({
    cellAlign: 'left',
    prevNextButtons: false,
    wrapAround: true,
    autoPlay: true,
    percentPosition: true,
    contain: true
});

smoothScroll.init({
    speed: 900,
    easing: 'easeInOutCubic',
    updateURL: false,
    offset: 50
});
