<?php get_header(); ?>
<?php the_post(); ?>
<div class="container-fluid">
    <div class="row">
        <article class="the-content col-md-12 no-paddingl no-paddingr">
            <?php $args = array('type' => 'image'); ?>
            <?php $img = rwmb_meta( 'rw_img_url', $args, get_the_ID()); ?>
            <?php foreach ( $img as $image ) { $back = $image['full_url']; } ?>
            <div class="parallax-image col-md-12" style="background:url('<?php echo $back; ?>')">
                <div class="parallax-text">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="the-post col-md-12">
                        <div class="the-cat">
                            <?php the_category(' '); ?>
                        </div>
                        <h2><?php the_date(); ?></h2>
                        <hr>
                        <?php the_content(); ?>
                        <div class="clearfix"></div>
                        <div class="the-cat">
                            <?php the_tags('Tags: ', ' ', ' '); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
<?php get_footer(); ?>
