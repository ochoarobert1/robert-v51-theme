<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/favicon.ico">
        <title><?php if(is_home()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?></title>
        <?php $current_url = $_SERVER['REQUEST_URI']; $clean_url = str_replace('/', '', $current_url); ?>
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = $my_posts[0]->post_excerpt; echo '<meta name="description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta name="description" content="Programador web en Venezuela, especializado en diseño y desarrollo de temas propios en wordpress, solución de problemas, optimización de procesos, SEO">'; } ?>
        <?php if (is_page() ) { echo '<meta name="description" content="Programador web en Venezuela, especializado en diseño y desarrollo de temas propios en wordpress, solución de problemas, optimización de procesos, SEO">'; } ?>
        <meta name="title" content="<?php if(is_home()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?>">
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $terms = get_the_terms( $my_posts[0]->ID, 'post_tag' ); if ( $terms && ! is_wp_error( $terms ) ) : $draught_links = array(); ?>
        <?php  foreach ( $terms as $term ) { $draught_links[] = $term->name; } $on_draught = join( ", ", $draught_links ); endif;
                               echo '<meta name="keywords" content="Robert Ochoa, Programador Web, Venezuela, '. $on_draught .'" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() || is_page() ) { echo '<meta name="keywords" CONTENT="Robert Ochoa, Programador Web, Venezuela, desarrollador web" />'; } ?>
        <meta name="robot" CONTENT="NOODP, INDEX, FOLLOW" />
        <meta name="author" CONTENT="Robert Ochoa" />
        <meta name="language" CONTENT="VE" />
        <meta name="geo.position" content="10.333333;-67.033333" />
        <meta name="ICBM" content="10.333333, -67.033333" />
        <meta name="geo.region" CONTENT="VE" />
        <meta name="geo.placename" CONTENT="Los Teques" />
        <meta name="DC.title" CONTENT="Robert Ochoa, Programador Web, Venezuela, desarrollador web" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@ochoarobert1" />
        <meta name="twitter:creator" content="@ochoarobert1" />
        <meta property='fb:admins' content='100000133943608' />
        <meta property="fb:app_id" content="611067105660122" />
        <meta property="og:title" content="<?php if(is_home()) { echo bloginfo("name"); echo " | "; echo 'Programador web, especializado en técnicas web'; } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?>" />
        <meta property="og:site_name" content="Robert Ochoa - Desarrollador Web" />
        <meta property="og:type" content="article" />
        <meta property="og:locale" content="es_ES" />
        <meta property="og:url" content="<?php if(is_single()) { the_permalink(); } else { echo 'http://robertochoa.com.ve/'; }?>" />
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = $my_posts[0]->post_excerpt; echo '<meta property="og:description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta property="og:description" content="Programador web en Venezuela, especializado en diseño y desarrollo de temas propios en wordpress, solución de problemas, optimización de procesos, SEO">'; } ?>
        <?php if (is_page() ) { echo '<meta property="og:description" content="Programador web en Venezuela, especializado en diseño y desarrollo de temas propios en wordpress, solución de problemas, optimización de procesos, SEO">'; } ?>
        <meta property="og:image" content='<?php if(is_single()){ $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; } else { echo bloginfo('template_url')."/images/oglogo.png"; } ?>' />
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head() ?>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/validationEngine.css" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/animate.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/flickity.min.css" />
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/evil-icons/1.7.2/evil-icons.min.css">
        <link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/robert-style.css" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/robert-mediaqueries.css" />
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '611067105660122',
                    xfbml      : true,
                    version    : 'v2.4'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <meta name="google-site-verification" content="0Dqsj-3AsnbcdKUEkVbGaKAUuKEvUimVysVfxT4Yj0Q" />
    </head>
    <div class="fb-admin"></div>
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PVS833"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PVS833');</script>
<!-- End Google Tag Manager -->
    <body <?php body_class() ?>>
        <div id="home" class="container-fluid">
            <div class="row">
                <?php if ( is_home() ) {?>
                <div class="the-hero col-md-12 no-paddingl no-paddingr">
                    <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Robert Ochoa - Logo" />
                    <h1>Robert Ochoa</h1>
                    <div class="col-md-2 col-md-offset-5"><hr></div>
                    <div class="clearfix"></div>
                    <h2>Programador especializado en soluciones Web</h2>
                </div>
                <?php } ?>
                <?php if ( is_single() ) {?>
                <div id="home" class="the-single-header col-md-12 no-paddingl no-paddingr">
                    <div class="col-md-4 ">
                        <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="logo Robert Ochoa" />
                    </div>
                    <div class="col-md-8 single-header-text">
                        <h1 class="text-center">Robert Ochoa</h1>
                        <div class="col-md-2 col-md-offset-5"><hr></div>
                        <div class="clearfix"></div>
                        <h2 class="text-center">Programador Web especializado en Wordpress</h2>
                    </div>
                </div>
                <?php } ?>
                <div id="sticker" class="the-navbar col-md-12 no-paddingl no-paddingr">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <?php if ( is_home() ) {?>
                                    <li><a href="#home" data-scroll>HOME</a></li>
                                    <li><a href="#resumen" data-scroll>RESUMEN</a></li>
                                    <li><a href="#servicios" data-scroll>SERVICIOS</a></li>
                                    <li><a href="#portafolio" data-scroll>PORTAFOLIO</a></li>
                                    <li><a href="#referencias" data-scroll>REFERENCIAS</a></li>
                                    <li><a href="#blog" data-scroll>BLOG</a></li>
                                    <li><a href="#contacto" data-scroll>CONTACTO</a></li>
                                    <?php } else { ?>
                                    <li><a href="<?php echo home_url('/'); ?>">HOME</a></li>
                                    <li><a href="<?php echo home_url('/'); ?>">RESUMEN</a></li>
                                    <li><a href="<?php echo home_url('/'); ?>">SERVICIOS</a></li>
                                    <li><a href="<?php echo home_url('/'); ?>">PORTAFOLIO</a></li>
                                    <li><a href="<?php echo home_url('/'); ?>">REFERENCIAS</a></li>
                                    <li><a href="<?php echo home_url('/'); ?>">BLOG</a></li>
                                    <li><a href="<?php echo home_url('/'); ?>">CONTACTO</a></li>
                                    <?php } ?>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div>
            </div>
        </div>
