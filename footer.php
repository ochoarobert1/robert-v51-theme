<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="the-footer col-md-12 no-paddingl no-paddingr">
                <h5 class="text-center">&copy; Robert Ochoa - Programador Web</h5>
                <h6 class="text-center">Todos los derechos reservados</h6>
                <div class="footer-icons col-md-12">
                    <a href="http://www.facebook.com/Robertkilgannon" target="_blank"><span class="social-sprites social-sprites-fb"></span></a>
                    <a href="https://twitter.com/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-tw"></span></a>
                    <a href="https://www.youtube.com/user/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-yt"></span></a>
                    <a href="https://instagram.com/ochoarob1/" target="_blank"><span class="social-sprites social-sprites-inst"></span></a>
                    <a href="https://ve.linkedin.com/in/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-link"></span></a>
                    <a href="https://www.behance.net/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-be"></span></a>
                    <a href="http://github.com/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-gh"></span></a>
                    <a href="skype:ochoa.robert1" target="_blank"><span class="social-sprites social-sprites-sk"></span></a>
                    <a href="https://plus.google.com/+RobertOchoa1/" target="_blank"><span class="social-sprites social-sprites-gp"></span></a>
                    <a href="https://es.pinterest.com/ochoarobert1/" target="_blank"><span class="social-sprites social-sprites-pin"></span></a>
                    <a href="https://soundcloud.com/robert8a" target="_blank"><span class="social-sprites social-sprites-sound"></span></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
<!-- JAVASCRIPT LOAD -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60621641-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.validationEngine-es.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.validationEngine.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.sticky.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/flickity.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/smooth-scroll.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/evil-icons/1.7.2/evil-icons.min.js"></script>
<!-- FUNCTIONS -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/functions.js"></script>
</body>
</html>
