<?php

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain('robertochoa', get_template_directory() . '/languages');
add_theme_support('post-thumbnails');
add_theme_support('menus');

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'robert_metabox' );

function robert_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'title'    => 'Media',
        'pages'    => array( 'post' ),
        'fields' => array(
            array(
                'name' => 'Parallax Image URL',
                'id'   => $prefix . 'img_url',
                'type' => 'image',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'    => 'Media',
        'pages'    => array( 'portafolio' ),
        'fields' => array(
            array(
                'name' => 'URL',
                'id'   => $prefix . 'url',
                'type' => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'    => 'Dirección URL de sitio web',
        'context'  => 'side',
        'priority' => 'high',
        'pages'    => array( 'referencias' ),
        'fields' => array(
            array(
                'name' => 'Sitio de Referencia URL',
                'id'   => $prefix . 'url-refferal',
                'type' => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'    => 'Evil Icon logo',
        'pages'    => array( 'servicio' ),
        'context'  => 'side',
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'EvilIcons',
                'id'   => $prefix . 'icons',
                'type' => 'select',
                'options'     => array(
                    'ei-gear' => __( 'ei-gear', 'ei-gear' ),
                    'ei-paperclip' => __( 'ei-paperclip', 'ei-paperclip' ),
                    'ei-location' => __( 'ei-location', 'ei-location' ),
                    'ei-lock' => __( 'ei-lock', 'ei-lock' ),
                    'ei-eye' => __( 'ei-eye', 'ei-eye' ),
                    'ei-cart' => __( 'ei-cart', 'ei-cart' ),
                )
            ),
        )
    );

    return $meta_boxes;
}

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

function portafolio() {

    $labels = array(
        'name'                => _x( 'Portafolio', 'Post Type General Name', 'robertochoa' ),
        'singular_name'       => _x( 'Item', 'Post Type Singular Name', 'robertochoa' ),
        'menu_name'           => __( 'Portafolio', 'robertochoa' ),
        'name_admin_bar'      => __( 'Portafolio', 'robertochoa' ),
        'parent_item_colon'   => __( 'Item Padre:', 'robertochoa' ),
        'all_items'           => __( 'Todos los Items', 'robertochoa' ),
        'add_new_item'        => __( 'Agregar Item', 'robertochoa' ),
        'add_new'             => __( 'Agregar', 'robertochoa' ),
        'new_item'            => __( 'Nuevo Item', 'robertochoa' ),
        'edit_item'           => __( 'Editar Item', 'robertochoa' ),
        'update_item'         => __( 'Actualizar Item', 'robertochoa' ),
        'view_item'           => __( 'Ver Item', 'robertochoa' ),
        'search_items'        => __( 'Buscar Item', 'robertochoa' ),
        'not_found'           => __( 'No encontrado', 'robertochoa' ),
        'not_found_in_trash'  => __( 'No encontrado en papelera', 'robertochoa' ),
    );
    $args = array(
        'label'               => __( 'portafolio', 'robertochoa' ),
        'description'         => __( 'Trabajos realizados', 'robertochoa' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-media-code',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'portafolio', $args );

}

// Hook into the 'init' action
add_action( 'init', 'portafolio', 0 );

function servicio() {

    $labels = array(
        'name'                => _x( 'Servicios', 'Post Type General Name', 'robertochoa' ),
        'singular_name'       => _x( 'Servicio', 'Post Type Singular Name', 'robertochoa' ),
        'menu_name'           => __( 'Servicios', 'robertochoa' ),
        'name_admin_bar'      => __( 'Servicios', 'robertochoa' ),
        'parent_item_colon'   => __( 'Servicio padre:', 'robertochoa' ),
        'all_items'           => __( 'Todos los servicios', 'robertochoa' ),
        'add_new_item'        => __( 'Agregar servicio', 'robertochoa' ),
        'add_new'             => __( 'Agregar', 'robertochoa' ),
        'new_item'            => __( 'Nuevo servicio', 'robertochoa' ),
        'edit_item'           => __( 'Editar servicio', 'robertochoa' ),
        'update_item'         => __( 'Actualizar servicio', 'robertochoa' ),
        'view_item'           => __( 'Ver servicio', 'robertochoa' ),
        'search_items'        => __( 'Buscar servicio', 'robertochoa' ),
        'not_found'           => __( 'No encontrado', 'robertochoa' ),
        'not_found_in_trash'  => __( 'No encontrado', 'robertochoa' ),
    );
    $args = array(
        'label'               => __( 'servicio', 'robertochoa' ),
        'description'         => __( 'Servicios ofrecidos', 'robertochoa' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-screenoptions',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'servicio', $args );

}

// Hook into the 'init' action
add_action( 'init', 'servicio', 0 );

function referencias() {

    $labels = array(
        'name'                => _x( 'Referencias', 'Post Type General Name', 'robertochoa' ),
        'singular_name'       => _x( 'Referencia', 'Post Type Singular Name', 'robertochoa' ),
        'menu_name'           => __( 'Referencias', 'robertochoa' ),
        'name_admin_bar'      => __( 'Referencias', 'robertochoa' ),
        'parent_item_colon'   => __( 'Referencia padre:', 'robertochoa' ),
        'all_items'           => __( 'Todas las Referencias', 'robertochoa' ),
        'add_new_item'        => __( 'Agregar referencia', 'robertochoa' ),
        'add_new'             => __( 'Agregar', 'robertochoa' ),
        'new_item'            => __( 'Nueva Referencia', 'robertochoa' ),
        'edit_item'           => __( 'Editar Referencia', 'robertochoa' ),
        'update_item'         => __( 'Actualizar Referencia', 'robertochoa' ),
        'view_item'           => __( 'Ver Referencia', 'robertochoa' ),
        'search_items'        => __( 'Buscar Referencia', 'robertochoa' ),
        'not_found'           => __( 'No encontrado', 'robertochoa' ),
        'not_found_in_trash'  => __( 'No encontrado', 'robertochoa' ),
    );
    $args = array(
        'label'               => __( 'referencias', 'robertochoa' ),
        'description'         => __( 'Compañeros con quienes trabajé', 'robertochoa' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-welcome-learn-more',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'referencias', $args );

}

// Hook into the 'init' action
add_action( 'init', 'referencias', 0 );

// Register Custom Taxonomy
function portafolio_taxonomy() {

    $labels = array(
        'name'                       => _x( 'portafolio_taxonomy', 'Taxonomy General Name', 'robertochoa' ),
        'singular_name'              => _x( 'Tipo de documento', 'Taxonomy Singular Name', 'robertochoa' ),
        'menu_name'                  => __( 'Tipo de documento', 'robertochoa' ),
        'all_items'                  => __( 'Todos', 'robertochoa' ),
        'parent_item'                => __( 'Padre', 'robertochoa' ),
        'parent_item_colon'          => __( 'Padre', 'robertochoa' ),
        'new_item_name'              => __( 'Nuevo', 'robertochoa' ),
        'add_new_item'               => __( 'Añadir', 'robertochoa' ),
        'edit_item'                  => __( 'Editar', 'robertochoa' ),
        'update_item'                => __( 'Actualizar', 'robertochoa' ),
        'view_item'                  => __( 'Ver', 'robertochoa' ),
        'separate_items_with_commas' => __( 'Separar por comas', 'robertochoa' ),
        'add_or_remove_items'        => __( 'Añadir o remover', 'robertochoa' ),
        'choose_from_most_used'      => __( 'Escoger de los mas usados', 'robertochoa' ),
        'popular_items'              => __( 'Populares', 'robertochoa' ),
        'search_items'               => __( 'Buscar', 'robertochoa' ),
        'not_found'                  => __( 'No encontrado', 'robertochoa' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'portafolio_taxonomy', array( 'portafolio' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'portafolio_taxonomy', 0 );
/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */

if (function_exists('add_image_size')) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
}

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

class BS3_Walker_Nav_Menu extends Walker_Nav_Menu {

    function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        $id_field = $this->db_fields['id'];

        if ( isset( $args[0] ) && is_object( $args[0] ) )
        {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );

        }

        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        if ( is_object($args) && !empty($args->has_children) )
        {
            $link_after = $args->link_after;
            $args->link_after = ' <b class="caret"></b>';
        }

        parent::start_el($output, $item, $depth, $args, $id);

        if ( is_object($args) && !empty($args->has_children) )
            $args->link_after = $link_after;
    }

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = '';
        $output .= "$indent<ul class=\"dropdown-menu list-unstyled\">";
    }
}

add_filter('nav_menu_link_attributes', 'nav_link_att', 10, 3);

function nav_link_att($atts, $item, $args) {
    if ( $args->has_children )
    {
        $atts['data-toggle'] = 'dropdown';
        $atts['class'] = 'dropdown-toggle';
    }
    return $atts;
}
