<?php get_header(); ?>
<div class="container-fluid">
    <div class="row">
        <section id="resumen" class="the-resume col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="resume-container col-md-12 no-paddingl no-paddingr">
                        <div class="col-md-7">
                            <h3>RESUMEN</h3>
                            <hr>
                            <p>En mis sitios siempre trato de combinar la estética con los aspectos funcionales, buscando el equilibrio adecuado para comunicarse sin molestar.</p>
                            <h2 class="text-center">¿Qué hace que un sitio web sea excelente?</h2>
                            <p class="profile-text">Aquí está mi opinión: un sitio web exitoso debe ser atractivo, pero fácil de leer. Un mensaje fascinante, junto con una comprensión inmediata, ademas de contenido original e información fácilmente rastreable: ¡esa es mi receta!</p>
                            <h3>Redes Sociales</h3>
                            <div class="col-md-10">
                                <a href="http://www.facebook.com/Robertkilgannon" target="_blank"><span class="social-sprites social-sprites-fb"></span></a>
                                <a href="https://twitter.com/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-tw"></span></a>
                                <a href="https://www.youtube.com/user/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-yt"></span></a>
                                <a href="https://instagram.com/ochoarob1/" target="_blank"><span class="social-sprites social-sprites-inst"></span></a>
                                <a href="https://ve.linkedin.com/in/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-link"></span></a>
                                <a href="https://www.behance.net/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-be"></span></a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-10">
                                <a href="http://github.com/ochoarobert1" target="_blank"><span class="social-sprites social-sprites-gh"></span></a>
                                <a href="skype:ochoa.robert1" target="_blank"><span class="social-sprites social-sprites-sk"></span></a>
                                <a href="https://plus.google.com/+RobertOchoa1/" target="_blank"><span class="social-sprites social-sprites-gp"></span></a>
                                <a href="https://es.pinterest.com/ochoarobert1/" target="_blank"><span class="social-sprites social-sprites-pin"></span></a>
                                <a href="https://soundcloud.com/robert8a" target="_blank"><span class="social-sprites social-sprites-sound"></span></a>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <img src="<?php bloginfo('template_url'); ?>/images/model.png" alt="Robert Ochoa - Foto" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="servicios" class="the-services col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="services-text">
                        <h2 class="text-center">SERVICIOS</h2>
                        <h3 class="text-center">Aqui te doy una descripción de todas las cosas en las que puedo ayudarte, siempre mejorando en cada una.</h3>
                        <div class="col-md-2 col-md-offset-5"><hr></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="services-container col-md-12 no-paddingl no-paddingr">
                        <?php $args = array('post_type' => 'servicio', 'posts_per_page' => 6, 'orderby' => 'date', 'order' => 'ASC'); ?>
                        <?php query_posts($args); $i=1; ?>
                        <?php while (have_posts()) : the_post() ?>
                        <div class="services-item services-item-<?php echo $i; ?> col-md-4 no-paddingl no-paddingr">
                            <div class="col-md-12">
                                <div class="col-md-3 no-paddingl">
                                    <div data-icon="<?php echo rwmb_meta('rw_icons'); ?>" data-size="l" class="service-icon"></div>
                                </div>
                                <div class="col-md-9 no-paddingr">
                                    <h2><?php the_title(); ?></h2>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                        <?php $i++; endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </section>

        <section id="portafolio" class="portfolio-section col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portfolio-text">
                            <h2 class="section-title text-center">PORTAFOLIO</h2>
                            <h4 class="section-desc text-center">Puedes ver varios de mis trabajos que con dedicación he cumplido, haz click para que veas una vista mas detallada.</h4>
                            <div class="col-md-2 col-md-offset-5"><hr></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="filter col-md-12">
                            <button id="all" data-filter="*"><span>TODOS</span></button>
                            <button id="desarrollo-web" data-filter=".desarrollo-web"><span>DESARROLLO WEB</span></button>
                            <button id="tienda-online" data-filter=".tienda-online"><span>TIENDA VIRTUAL</span></button>
                        </div>
                    </div>
                    <div id="portafolio-container" class="portfolio-container col-md-12 no-paddingl no-paddingr">
                        <?php $args = array('post_type' => 'portafolio', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC'); ?>
                        <?php query_posts($args); ?>
                        <?php while (have_posts()) : the_post() ?>
                        <?php $terms = get_the_terms( $post->ID, 'portafolio_taxonomy' ); if ( $terms && ! is_wp_error( $terms ) ) : $draught_links = array(); ?>
                        <?php foreach ( $terms as $term ) { $draught_links[] = $term->slug; } $on_draught = join( " ", $draught_links ); ?>
                        <div id="<?php echo get_the_ID(); ?>" class="portfolio-item col-md-4 <?php echo $on_draught; ?>">
                            <div class="portfolio-img">
                                <div class="portfolio-mask">
                                    <p><?php the_excerpt(); ?></p>
                                </div>
                                <?php the_post_thumbnail('portafolio', array('class' => 'img-responsive')); ?>
                            </div>
                            <p class="portfolio-title"><?php the_title(); ?></p>

                        </div>
                        <?php endif; ?>
                        <?php $i++; endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </section>

        <section id="referencias" class="references-section col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section-title text-center">Compañeros con quienes trabajo/trabajé</h2>
                        <h5 class="section-desc text-center">Personas que han colaborado en mi crecimiento personal / profesional / espiritual</h5>
                        <div class="col-md-2 col-md-offset-5"><hr></div>
                        <div class="clearfix"></div>
                        <div class="reference-gallery col-md-12">
                            <?php $args = array('post_type' => 'referencias', 'posts_per_page' => -1); ?>
                            <?php query_posts($args); ?>
                            <?php while (have_posts()) : the_post() ?>
                            <div class="references-item">
                                <div class="reference-photo col-md-4">
                                    <?php the_post_thumbnail('ref-avatar'); ?>
                                </div>
                                <div class="reference-info col-md-8">
                                    <h2 class="reference-name"><?php the_title(); ?></h2>
                                    <div class="col-md-5 no-paddingl"><hr></div>
                                    <div class="clearfix"></div>
                                    <div class="reference-work"><?php the_excerpt(); ?></div>
                                    <div class="reference-desc"><?php the_content(); ?></div>
                                    <a href="<?php echo rwmb_meta('rw_url-refferal'); ?>" target="_blank"><div data-icon="ei-link" data-size="m" class="pull-right"></div></a>
                                </div>
                            </div>

                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="blog" class="blog-section col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 no-paddingl no-paddingr">
                        <h2 class="section-title text-center">El Blog</h2>
                        <div class="col-md-2 col-md-offset-5"><hr></div>
                        <div class="clearfix"></div>
                        <div class="blog-container col-md-12 no-paddingl no-paddingr">
                            <?php $args = array('posts_per_page' => 6); ?>
                            <?php query_posts($args); ?>
                            <?php while (have_posts()) : the_post() ?>
                            <div class="blog-item col-md-4">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="blog-img col-md-12 no-paddingl no-paddingr">
                                        <div class="blog-img-mask"></div>
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_img'); ?></a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="blog-item-text">
                                        <h5 class="blog-category text-center"><?php the_category(' '); ?></h5>
                                        <a href="<?php the_permalink(); ?>"><h2 class="blog-title text-center"><?php the_title();?></h2></a>
                                    </div>
                                </a>
                            </div>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <section id="contacto" class="contact-section col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section-title text-center">Contacto</h2>
                        <p class="section-desc text-center">Mandame un correo que me pondré en contacto contigo lo antes posible</p>
                        <div class="col-md-2 col-md-offset-5"><hr></div>
                        <div class="clearfix"></div>
                        <div class="contact-container col-md-12">
                            <?php get_template_part('contacto-submit') ?>
                            <form action="./" method="post" class="form margint30" role="form" id="contact-form">
                                <div class="form-group">
                                    <input type="text" class="form-control validate[required]" id="nombre" name="nombre" placeholder="Nombre *">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control validate[required,custom[number]]" id="telefono" name="telefono" placeholder="Tel&eacute;fono de Contacto">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control validate[required,custom[email]]" id="email" name="email" placeholder="Correo electrónico *">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control validate[required]" id="comentarios" name="comentarios" placeholder="Comentarios *"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success btn-lg pull-right" id="btn_submit" value="ENVIAR COMENTARIOS">
                                </div>
                                <div id="form-submit-wrapper"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
    get_footer();
