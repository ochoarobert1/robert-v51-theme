<?php get_header(); ?>
<?php the_post(); ?>
<div class="container-fluid">
    <div class="row">
        <div class="the-content col-md-12">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section-title text-center">El Blog</h2>
                        <div class="col-md-2 col-md-offset-5"><hr></div>
                        <div class="clearfix"></div>
                        <div class="blog-container col-md-12 no-paddingl no-paddingr">
                            <?php $args = array('posts_per_page' => 3); ?>
                            <?php query_posts($args); ?>
                            <?php while (have_posts()) : the_post() ?>
                            <div class="blog-item col-md-4">
                                <div class="blog-img col-md-12 no-paddingl no-paddingr">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog_img'); ?></a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="blog-item-text">
                                    <h5 class="blog-category text-center"><?php the_category(' '); ?></h5>
                                    <a href="<?php the_permalink(); ?>"><h2 class="blog-title text-center"><?php the_title();?></h2></a>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
